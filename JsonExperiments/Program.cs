﻿using System;
using System.Collections;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace JsonExperiments
{
	class Program
	{
		static void Main(string[] args)
		{
			var jsonDog = @"{
				""Pet"": {
					""Name"": ""Fido"",
					""Weight"": 80
				},
				""PetIsDog"": true
			}";
			var jsonCat = @"{
				""Pet"": {
					""Name"": ""Fluffy"",
					""Temperament"": ""grouchy""
				},
				""PetIsDog"": false
			}";
			var options = new JsonSerializerOptions();
			options.Converters.Add(new ContainerConverter());

			var animalContainer = JsonSerializer.Deserialize<Container>(jsonDog); // no options are passed so the converter is not included.
			Console.WriteLine("Animal test: " + animalContainer.Pet);
			var dogContainer = JsonSerializer.Deserialize<Container>(jsonDog, options);
			Console.WriteLine("Dog test: " + dogContainer.Pet);
			var catContainer = JsonSerializer.Deserialize<Container>(jsonCat, options);
			Console.WriteLine("Cat test: " + catContainer.Pet);
		}
	}



	class ContainerConverter : JsonConverter<Container>
	{
		public override Container Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			using JsonDocument document = JsonDocument.ParseValue(ref reader);
			var root = document.RootElement;
			var petIsDogProp = root.EnumerateObject().FirstOrDefault(p => p.Name == "PetIsDog");
			var petProp = root.EnumerateObject().FirstOrDefault(p => p.Name == "Pet");
			var toReturn = JsonSerializer.Deserialize<Container>(root.ToString());
			if (petIsDogProp.Value.ValueKind == JsonValueKind.True)
			{
				toReturn.Pet = JsonSerializer.Deserialize<Dog>(petProp.Value.ToString());
			}
			else
			{
				toReturn.Pet = JsonSerializer.Deserialize<Cat>(petProp.Value.ToString());
			}
			return toReturn;
		}

		public override void Write(Utf8JsonWriter writer, Container value, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}
	}

	class Container
	{
		public Animal Pet { get; set; }
		public bool PetIsDog { get; set; }
	}

	class Animal
	{
		public string Name { get; set; }
		public override string ToString()
		{
			return $"Animal with the name of {Name}";
		}
	}

	class Dog : Animal
	{
		public int Weight { get; set; }
		public override string ToString()
		{
			return $"Dog with the name of {Name}, and Weight of {Weight}";
		}
	}

	class Cat: Animal
	{
		public string Temperament { get; set; }
		public override string ToString()
		{
			return $"Cat with the name of {Name}, and Temperament of {Temperament}";
		}
	}
}
